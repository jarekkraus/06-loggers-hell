package uj.jwzp2019.loghell;

import com.external.PaymentsService;
import com.internal.DiscountCalculator;
import org.apache.commons.cli.*;
import java.math.BigDecimal;

public class TicketPurchase {

   public static void main(String[] args) {DiscountCalculator discountCalculator = new DiscountCalculator();
        PaymentsService paymentsService = new PaymentsService();

        CommandLineParser parser = new DefaultParser();
        Options options = getOptions();

        try {
            CommandLine commandLine = parser.parse(options, args);
            if (commandLine.hasOption("h")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("CommandLineParameters", options);
            }

            BigDecimal ticketPrice = new BigDecimal(commandLine.getOptionValue("ticketPrice"));
            int customerAge = Integer.parseInt(commandLine.getOptionValue("customerAge"));
            int customerId = Integer.parseInt(commandLine.getOptionValue("customerId"));
            int companyId = Integer.parseInt(commandLine.getOptionValue("companyId"));

            discountCalculator.calculateDiscount(ticketPrice, customerAge);
            paymentsService.makePayment(customerId, companyId, ticketPrice);

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    private static Options getOptions() {
        Options options = new Options();
        options.addOption("price","ticketPrice",true, "Ticket price");
        options.addOption("age","customerAge", true, "Customer age");
        options.addOption("id","customerId", true, "Customer id");
        options.addOption("compId","companyId", true, "Company id");
        options.addOption("h", "help", false, "Shows this help");
        return options;
    }
}
